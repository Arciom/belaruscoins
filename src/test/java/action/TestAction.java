package action;

import org.junit.Assert;
import org.junit.Test;
import static action.DateConversion.convertCatalogDate;
import static action.DateConversion.convertEbayDate;

/**
 * Data conversion tests.
 */
public class TestAction {

    @Test
    public void testConvertCatalogDate() {

        System.out.println("date test");
        String testDate = "07 April 2010";
        Assert.assertEquals("2010-04-07", convertCatalogDate(testDate));
    }

    @Test
    public void testconvertEbayDate() {

        String testDate = "Nov-07";
        Assert.assertEquals("2016-11-07", convertEbayDate(testDate));
        String testDate2 = "27.10 ";
        Assert.assertEquals("2016-10-27", convertEbayDate(testDate2));
    }

}



