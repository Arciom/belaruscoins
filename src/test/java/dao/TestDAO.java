package dao;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class TestDAO {
  
    @Autowired
    private CoinsDAOFactory coinsFactory;

    @Autowired
    private CoinDAO coinDAO;

    @Autowired
    private PriceDAO priceDAO;

    @Test
    public void testCoinDAO() {

        // test get all
        List list = coinDAO.getAllCoins();
        Assert.assertNotNull(list);
        Assert.assertTrue(list.size() > 0);

   }

   @Test
   public void testPriceDAO() {
      
        // test get all
        List list = priceDAO.getAllPrices();
        Assert.assertNotNull(list);
        Assert.assertTrue(list.size() > 0);
   }

}
