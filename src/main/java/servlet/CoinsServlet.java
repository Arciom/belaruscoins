package servlet;

import dao.CoinDAO;
import dao.PriceDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import com.google.gson.Gson;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ContextConfiguration("/applicationContext.xml")
public class CoinsServlet extends HttpServlet{

    @Autowired
    private CoinDAO coinDAO;
    @Autowired
    private PriceDAO priceDAO; 

    @Override
    public void init(ServletConfig config) throws ServletException {
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
        super.init(config);
    }

    @Override
    public final void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            doRequest(req, res);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public final void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            doRequest(req, res);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void doRequest(HttpServletRequest req, HttpServletResponse res) throws Throwable {
        try {
            System.out.println("Servlet+++++++++++++++++");
            res.setContentType("application/json");
            res.setCharacterEncoding("UTF-8");

            int cId = 0;
            String buttonId = req.getParameter("buttonId");
            //System.out.println("bbb-" +  buttonId);

            String tableId = req.getParameter("tableId");
            //System.out.println("dddd-" + tableId);
            if (tableId !=null) {
                cId = Integer.parseInt(req.getParameter("cId"));
            }

            if ("coin2".equals(buttonId)) {
                String json = new Gson().toJson(coinDAO.getAllCoins());
                res.getWriter().write(json);

            } else if ("selectedCoins".equals(buttonId)) {
                String json="";
                String metal = req.getParameter("metalSc");
                String year = req.getParameter("yearSc");
                String denom = req.getParameter("denomSc");
                System.out.println("yyyyy-" + year + ";");
                if ("Any".equals(metal)){
                    if ("Any".equals(denom)){
                        if (year != null){
                            json = new Gson().toJson(coinDAO.getCoinsByYear(year));
                        } // else all data null - call show all?
                    } else if ("".equals(year)) {
                               json = new Gson().toJson(coinDAO.getCoinsByDenomination(Integer.parseInt(denom)));
                           } else {
                               json = new Gson().toJson(coinDAO.getCoinsByYearDenomination(year, Integer.parseInt(denom)));
                           }
                } else if ("Any".equals(denom)){
                           if ("".equals(year)){
                               json = new Gson().toJson(coinDAO.getCoinsByMetal(metal));
                           } else {
                               json = new Gson().toJson(coinDAO.getCoinsByMetalYear(metal, year));
                           }
                       } else if ("".equals(year)) {
                           json = new Gson().toJson(coinDAO.getCoinsByMetalDenomination(metal, Integer.parseInt(denom)));
                         } else {
                             json = new Gson().toJson(coinDAO.getCoinsByMetalYearDenomination(metal, year, Integer.parseInt(denom)));
                }

                res.getWriter().write(json);
            }

            if ("coinsTableRow".equals(tableId)) {
                String json = new Gson().toJson(priceDAO.getPricesByCoinId(cId));
                res.getWriter().write(json);
            }

        } catch(Exception e) {
            throw new ServletException("Error in the servlet", e);
            //  PrintWriter pw = res.getWriter();
            //  pw.println(e.getMessage());
        }
    }
}