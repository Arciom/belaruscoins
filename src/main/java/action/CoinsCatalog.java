package action;

import coins.Coin;
import dao.CoinDAO;
import dao.CoinsDAOFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses coins data from the catalog site.
 * Saves coins to DB.
 */
//@ContextConfiguration("/applicationContext.xml")
public class CoinsCatalog implements JsoupConnection {

    //public static final String CATALOG_URL = "C:\\test\\000.htm";
    public static final String CATALOG_URL = "http://www.nbrb.by/engl/CoinsBanknotes/Coins/Commemorative/Catalog";
    public static final String IMAGE_URL = "http://www.nbrb.by/CoinsBanknotes/images/?id=";
    public static final String IMAGE_STORAGE = "C:\\test\\storage\\coins\\";
/*
    @Autowired
    private CoinDAO coinDAO;
*/
     /**
      * |Getting:
      * 1) coin links data from the catalog page + coin of coin
      * 2) than denomination, description, images
      * 3) insert ready coin to the table
      *
      * @param catalogLink an URL of the catalog of nbrb.by coins location
      */
    public void getCoinsData(String catalogLink) throws Throwable {

        Coin coin = new Coin();
        //File input = new File(catalogLink);
        //Document doc = Jsoup.parse(input, "UTF-8");
        // Jsoup connection
        Document doc = getDocument(catalogLink);
        // in these row classes nbrb.by has data (denoination, description, images) (<tr class="streak">)
        String[] rowClasses = { "streak", "bottomstreak"};
        // links are in the element <ul class=withtit>
        Elements allLinksFromClass = doc.select("[class=withtit]");
        Elements allLinks = allLinksFromClass.select("a[href]");
        int i = 1;
        for (Element link : allLinks) {
            // the link
            String href = link.attr("href");
            System.out.println(":" + href + "---" + href);
            // coin name
            coin.setCoinName(link.text());
            // call method to collect other data (denomination, description, images)
            extractCoinDescription("http://www.nbrb.by/engl/" + href, rowClasses, coin);
           // TimeUnit.SECONDS.sleep(3);
           //if (i++ > 1) break;
        }
    }
    /**
     * Getting coin data (denomination, description, images) from the catalog page
     * description are in the nbrb.by tables rows with class "streak" & "bottomstreak"
     * @param
     * @return
     */
    private void extractCoinDescription(String coinLink, String[] rowClasses, Coin coin2) throws Throwable {

        CoinsDAOFactory factory = new CoinsDAOFactory();
        CoinDAO coinDAO = factory.getCoinDAO();
        //File input = new File(coinLink);
        //Document doc = Jsoup.parse(input, "UTF-8");
        // Jsoup connection
        Document doc = getDocument(coinLink);
        // text about the coin to extract the date of the coin
        String textAtCoinPage = doc.getElementById("BodyHolder_fvMain").text();
        // Create a Pattern object
        // the date is like 02 April 1997: two digits(day), world(month), four digits(year)
        String coinDate = "";
        Pattern yearPattern = Pattern.compile("\\d{2}[ ][a-zA-Z]+[ ]\\d{4}");
        // Now create matcher object
        Matcher yearMatcher = yearPattern.matcher(textAtCoinPage);
        if (yearMatcher.find()) {
            //System.out.println("Found value:" + yearMatcher.group() );
            coinDate = yearMatcher.group(0);
        }
        // System.out.println(convertCatalogDate(coinDate));
        coin2.setCoinDate(DateConversion.convertCatalogDate(coinDate));

        // get coinInfo from array rowClasses {"streak", "bottomstreak" }
        for (String tag : rowClasses) {

            Elements coinTable = doc.getElementsByClass(tag);
            for (Element data : coinTable) {
                // getting the denomination cleaning it then get 1,000
                String denomination = data.text().substring(data.text().indexOf("Denomination: ") +
                                                            "Denomination: ".length(), data.text().indexOf(" ruble")) ;
                denomination = denomination.replaceAll("\\D+", "");
                //System.out.println("Denomination=" + denomination + "ok");
                coin2.setDenomination(Integer.parseInt(denomination));

                // save HTML with description of the coin   set - BodyHolder_fvMain_lvCoins_lvSets_0_itemPlaceholderContainer_0"
                Elements description = data.select("[style=padding-top: 6px; padding-bottom: 6px;vertical-align:top]");
                //System.out.println(htmlDescription.toString());
                coin2.setDescription(description.toString());

                // getting picture link from <a> tag
                Elements coinImg = data.getElementsByTag("a");
                for (Element img : coinImg) {
                    System.out.println("00--" + img.html());
                    // obverse img has id="BodyHolder_fvMain_lvCoins_hlObverse_0 (1 or 2)"
                    // reverse img has id="BodyHolder_fvMain_lvCoins_hlReverse_0 (1 or 2)"
                    if (img.attr("id").contains("BodyHolder_fvMain_lvCoins_hlObverse")) {
                        //System.out.println("id--" + img.attr("id"));
                        //System.out.println("obv--" + img.attr("data-imageid"));
                        coin2.setImageObverse(img.attr("data-imageid") + ".gif");
                        downloadCoinImage(IMAGE_URL + img.attr("data-imageid"),
                                          IMAGE_STORAGE + img.attr("data-imageid") + ".gif");

                    } else if (img.attr("id").contains("BodyHolder_fvMain_lvCoins_hlReverse")) {
                        //System.out.println("rev--" + img.attr("data-imageid"));
                        coin2.setImageReverse(img.attr("data-imageid") + ".gif");
                        downloadCoinImage(IMAGE_URL + img.attr("data-imageid"),
                                          IMAGE_STORAGE + img.attr("data-imageid") + ".gif");
                    }
                }
                // when all data are collected, insert the coin to the Coin table
                try {
                    coinDAO.insertCoin(coin2);
                } catch (Exception e) {
                    System.err.println("Caught Exception while coin saving: " + e.getMessage());
                }
            }
        }
    }

    /**
     * Downloading the coin image to the selected storage.
     * @param imageUrl URL of an image.
     * @param destination where to save an image.
     * @throws Throwable MalformedURLException
     */
    public static void downloadCoinImage(String imageUrl, String destination) throws Throwable {

        URL website = new URL(imageUrl);
        try(
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());
                FileOutputStream fos = new FileOutputStream(destination);
        ) {

            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (IOException e) {
            System.err.println("Caught IOException downloading of the coin image: " + e.getMessage());
        }
    }
// end of class
}