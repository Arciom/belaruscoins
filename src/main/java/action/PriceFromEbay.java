package action;

import coins.Coin;
import coins.Price;
import dao.CoinDAO;
import dao.CoinsDAOFactory;
import dao.PriceDAO;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  Coins that were sold at Ebay.
 */
public class PriceFromEbay implements JsoupConnection {

    public static final String COINS_GRADE[] = {"proof", "prooflike", "proof-like", " BU ", "brilliant uncirculated",
                                                " UNC ", "uncirculated", " EF ", " XF ", "extremely fine",
                                                " VF ", "very fine"};
    /**
     * Getting items from ebay pages with ended auctions.
     * @param auLink auction link
     */
    public void getPriceData(String auLink) {
        try {
            // searching prices on ebay page by page
            int j = 1;
            int numberOfPages;
            do {
                // Jsoup connection
                Document doc = getDocument(auLink);
                //number of the pages with results (200 items on page) class=rcnt  could be "1 333"
                numberOfPages = Integer.parseInt(doc.select("[class=rcnt]").text().replaceAll("\\D+", ""))/200 ;
                //System.out.println(numberOfPages);
                // data are in the element <li class=sresult lvresult clearfix li>
                Elements allSoldEbayItems = doc.select("[class=sresult lvresult clearfix li]");
                //System.out.println(allSoldEbayItems);
                //int i=0;
                for (Element auItem33 : allSoldEbayItems) {

                    getEbayPrice(auItem33);
                    //TimeUnit.SECONDS.sleep(1);
                    //if (i++ > 2) break;
                }
                // make second page link, and so on...
                // nkw=belarus&rt=nc&_ipg=200  nkw=belarus&rt=nc&_pgn=2&_skc=200 nkw=belarus&rt=nc&_pgn=3&_skc=400
                 j++;
                 auLink = auLink.replace(auLink.substring(auLink.indexOf("=nc&")+4),
                          "pgn=" + Integer.toString(j) + "&_skc=" + Integer.toString(200*(j-1)));
                 System.out.println("!!!!!!!!" +  auLink);
            } while (j <= numberOfPages + 1);

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * Getting all data and saving price to DB.
     * @param auItem auction element from sold items page.
     */
    public void getEbayPrice(Element auItem) {

        CoinsDAOFactory factory = new CoinsDAOFactory();
        CoinDAO coinDAO = factory.getCoinDAO();
        PriceDAO pd = factory.getPriceDAO();

        try {
            // data are in the element <h3 class=lvtitle>
            // auName - au description has coin and price data
            // if ebay page gives not english interface look at <h3> fist child tag <a data-mtdes=>
            String auNameStr = " ";
            Elements auName = auItem.select("[class=lvtitle]");
            Elements auNameEng = auName.select("a[data-mtdes]");
            //System.out.println("+++:" + auName);
            //System.out.println("+++22:" + auNameEng);
            if (auNameEng.hasAttr("data-mtdes")) {
                auNameStr = auNameEng.attr("data-mtdes");
            } else {
                auNameStr = auName.text();
            }
            System.out.println("ReadyName:" + auNameStr);

            //bids from class=vip visited
            Elements auBids = auItem.select("[class=lvformat]");
            // au type can be only classic
            String bids = "no bids";
            if (auBids.text().contains("bid") || auBids.text().contains("ставк") ) {
                // leave only number of bids
                bids = auBids.text().replaceAll("\\D+", "");
            }
            //System.out.println("bids=" + bids);

            // in au description must be "belarus" & not "set " we need only single coin
            // bids must be not "no bids" to exactly know the coin price
            if (auNameStr.toLowerCase().contains("belarus") &&
                !auNameStr.toLowerCase().contains("set ") &&
                !bids.equals("no bids")) {
                // get  year & denomination for defining a coin from catalog
                // sometimes in au description there are no year or denomination so it's hard to determine the coin
                // skip this lot - set "0"
                String auYear = "0", auDenomination = "0";
                // create a Pattern object & matcher object
                // year must have: four digits between space or coma, or at the beginning of string
                Pattern yearPattern = Pattern.compile("[ ,]?\\d{4}[ ,]");
                Matcher yearMatcher = yearPattern.matcher(auNameStr);
                if (yearMatcher.find()) {
                    //System.out.println("Found value:" + yearMatcher.group() );
                    auYear = yearMatcher.group().replaceAll("\\D+", "");
                }
                //System.out.println("year:" + auYear);

                // denomination must have: one ore more digit, space ore not, no case sensitive
                // 200R Belarusian 2008, 1 rouble Ballet Gold Coin , 1 RUBLE 1997
                Pattern denomPattern = Pattern.compile("\\d+[ ]?((?i)r[o]?[u]?[b]?)");
                Matcher denomMatcher = denomPattern.matcher(auNameStr);
                if (denomMatcher.find()) {
                    //System.out.println("Denom value:" + denomMatcher.group());
                    // leave only digits
                    auDenomination = denomMatcher.group().replaceAll("\\D+", "");
                }
                //System.out.println(auDenomination);
                // get list of coins with the same year & denomination
                List<Coin> listCoin = coinDAO.getCoinsByYearDenomination(auYear, Integer.parseInt(auDenomination));
                //System.out.println(listCoin);
                for (Coin coin : listCoin) {
                    // get au coin and price data
                    // au description must containe real name of the coin, than add the price to DB
                    if (auNameStr.toLowerCase().contains(coin.getCoinName().toLowerCase())) {
                        System.out.println("coinId=" + coin.getId());
                        // price from class=lvprice prc, delete from ru version "US "
                        Elements auPrice = auItem.select("[class=lvprice prc]");
                        //System.out.println("price:" + auPrice.text());
                        String auPriceStr = auPrice.text();
                        if (auPriceStr.contains("US ")) {
                            auPriceStr = auPriceStr.replace("US ", "");
                        }
                        //System.out.println("--price:" + auPriceStr);
                        // date from class=tme, to be converted convertEbayDate(auDate.text())
                        Elements auDate = auItem.select("[class=lvdetails left space-zero full-width]");
                        System.out.println("date:" + auDate.text());
                        // au href
                        Elements auHref = auItem.select("a[href]");
                        String auHrefStr = auHref.attr("href");
                        System.out.println(auHrefStr);
                        // get grade And Image
                        String gradeAndImage[] = getEbayGradeAndImage(auHrefStr, coin.getId());
                        // save price to DB price, grade, date, bids, link, image, coinId
                        pd.insertPrice(new Price(1, auPriceStr,
                                gradeAndImage[0],
                                DateConversion.convertEbayDate(auDate.text()),
                                bids,
                                auHref.attr("href"),
                                gradeAndImage[1],
                                coin.getId()));
                        System.out.println("price ********************** saved");
                        break;
                    }
                }
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    /**
     * Getting grade and image.
     * @param goForGradeAndImage
     * @param coinId coin Id for image naming
     * @return grade adn image in an array
     * @throws Throwable
     */
    public String[] getEbayGradeAndImage(String goForGradeAndImage, int coinId) {

        String auGrade = "not specified";
        String imgFileName = coinId + "coin";

        Properties props = new Properties();
        InputStream input = null;

        try {
            // go into usual ebay sold item page for grade & image
            // but sometimes ebay au page, go to link at
            // Jsoup connection
            Document goToAu = getDocument(goForGradeAndImage);
            Elements inAuLink = goToAu.select("[class=vi-cvip-dummyPad20]");
            //System.out.println("11--:" + inAuLink.text() + ";");
            // loading properties file
            input = PriceFromEbay.class.getResourceAsStream("/blrcoins.properties");
            props.load(input);

            if (!inAuLink.isEmpty()) {
                // its usual ebay sold item page
                // grade from class="vi-cvip-dummyPad20"
                auGrade = getGrade(inAuLink);
                System.out.println("11Grade--" + auGrade);

                // image from  span class=vi-inl-lnk vi-cvip-prel3 in child <a>
                //                        vi-inl-lnk vi-cvip-prel5
                // value starting with "vi-inl-lnk vi-cvip-prel"
                Elements inAuLinkImage = goToAu.select("[class^=vi-inl-lnk vi-cvip-prel");
                System.out.println("44-" + inAuLinkImage.select("a[href]").attr("href"));
                // go into au "See original listing" for image

                // Jsoup connection
                Document goToOriginalAu = getDocument(inAuLinkImage.select("a[href]").attr("href"));
                // image from id=icImg
                Element auImg = goToOriginalAu.getElementById("icImg");
                System.out.println("55-" + auImg.attr("src"));
                // img file name (coinId+time.jpg) for saving to disk
                imgFileName += LocalDateTime.now().toString().replaceAll("\\D+", "") + ".jpg";
                // save to disk
                CoinsCatalog.downloadCoinImage(auImg.attr("src"),
                                               props.getProperty("prisesImageStorage") + imgFileName);
            } else {
                // its RARE ebay sold item page, we are at once at the full item page "See original listing"
                // find grade itemAttr
                auGrade = getGrade(goToAu);
                System.out.println("rare-Grade--" + auGrade);

                // image from id=icImg
                Element auImg = goToAu.getElementById("icImg");
                System.out.println("rare+" + auImg.attr("src"));
                // img file name for saving to disk
                imgFileName += LocalDateTime.now().toString().replaceAll("\\D+", "") + ".jpg";
                // save to disk
                CoinsCatalog.downloadCoinImage(auImg.attr("src"),
                                               props.getProperty("prisesImageStorage") + imgFileName);
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return new String[] {auGrade, imgFileName};
    }

    /**
     * Finding a grade at the au page.
     * @param grade Element to be looked for grade.
     * @return the coin grade
     */
    public String getGrade(Elements grade) {
        String cGrade = "not specified";
        // finding a grade in the text comparing with COINS_GRADE array
        for (String s : COINS_GRADE) {
            if (grade.text().toLowerCase().contains(s.toLowerCase())) {
                cGrade = s;
                //System.out.println(s + "--auGrade--" + cGrade);
                break;
            }
        }
        // sometimes in a frame, frame link in element with id=desc_ifr
        // if id=desc_ifr is exist
        if (cGrade.equals("not specified") && grade.select("[id=desc_ifr]").size()!=0) {
            Elements inFrame = grade.select("[id=desc_ifr]");
            //System.out.println("rare-1133--:" + inFrame + ";");
            Document goToFrame = getDocument(inFrame.attr("src"));
            cGrade = getGrade(goToFrame);
            //System.out.println("rare-22Grade--" + cGrade);
        }
        return cGrade;
    }

    /**
     * Finding a grade at the au page.
     * @param grade Document to be looked for grade.
     * @return the coin grade
     */
    public String getGrade(Document grade) {
        String cGrade = "not specified";
        // finding a grade in the text comparing with COINS_GRADE array
        for (String s : COINS_GRADE) {
            if (grade.text().toLowerCase().contains(s.toLowerCase())) {
                cGrade = s;
                //System.out.println(s + "--auGrade--" + cGrade);
                break;
            }
        }
        // sometimes in a frame, if id=desc_ifr is exist
        if (cGrade.equals("not specified") && grade.select("[id=desc_ifr]").size()!=0) {
            Elements inFrame = grade.select("[id=desc_ifr]");
            //System.out.println("rare-1133--:" + inFrame + ";");
            Document goToFrame = getDocument(inFrame.attr("src"));
            cGrade = getGrade(goToFrame);
            //System.out.println("rare-22Grade--" + cGrade);
        }
        return cGrade;
    }
// class end
}