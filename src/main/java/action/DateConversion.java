package action;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Date conversion class.
 */
public class DateConversion {

    /**
     * Convert date from catalog site to sql date format yyyy-mm-dd.
     * @param date the date to convert.
     * @return converted date.
     */
    public static String convertCatalogDate(String date) {

        Date dateTemp;
        DateFormat dateFormatDb = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateCatalogFormat = new SimpleDateFormat("dd MMMMM yy");
        try {
            dateTemp = dateCatalogFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "1991-01-01";
        }

        return dateFormatDb.format(dateTemp);
    }

    /**
     * Converts ebay date to sql date format yyyy-mm-dd.
     * @param date the date to convert.
     * @return converted date.
     */
    public static String convertEbayDate(String date) {

        Date dateTemp;
        DateFormat dateFormatDb = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateEbayFormat = new SimpleDateFormat("MMM-dd-yyyy");
        // Nov-08 23:40 delete unnecessary part with time
        // 30.10 02:46  russian ebay version...
        String newDate = date.substring(0,6) + "-" + LocalDateTime.now().getYear() ;

        if (newDate.charAt(5) == ' ') {
            dateEbayFormat = new SimpleDateFormat("dd.MM -yyyy");
        }
        try {
            dateTemp = dateEbayFormat.parse(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "1991-01-01";
        }
        return dateFormatDb.format(dateTemp);
    }

}
