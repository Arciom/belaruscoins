package action;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.io.IOException;

/**
 * Jsoup connection.
 */
public interface JsoupConnection {

    default Document getDocument(String url) {
        try {
            Document doc = Jsoup.connect(url)
                     //.userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0")
                     .header("Accept-Language", "en")
                     .referrer("http://www.google.com")
                     .followRedirects(true)
                     //.timeout(1)
                     .get();
            return doc;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
