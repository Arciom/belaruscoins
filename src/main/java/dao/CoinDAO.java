package dao;

import coins.Coin;
import hibernate.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;

public class CoinDAO {

    private Session session;

    public CoinDAO() {
    }

    // adding Coin
    public void insertCoin(Coin coin) {

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(coin);
            session.getTransaction().commit();

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }
    // deleting the Coin by id
    public void deleteCoin(int id) {

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Coin result = (Coin) session.createCriteria(Coin.class)
                    .add(Restrictions.idEq(id))
                    .uniqueResult();

            if (result != null){
                session.delete(result);
                session.getTransaction().commit();
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    // updating the coin
    public void updateCoin(Coin coin) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(coin);
            session.getTransaction().commit();

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    // get all coins
    public List <Coin> getAllCoins() {
        List <Coin> coins = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            coins = session.createCriteria(Coin.class).list();

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
       // System.out.println(coins.getClass());
        return coins;
    }

    // get coin by year
    public List<Coin> getCoinsByYear(String date) {
        List<Coin> listCoins = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // select from Coins where coin_date = date
            String hql = "from Coin where YEAR(coinDate) = " + date;
            Query query = session.createQuery(hql);
            listCoins = query.list();

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return listCoins;
    }

    // get coin by denomination
    public List<Coin> getCoinsByDenomination(int denomin) {
        List<Coin> listCoins = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // select from Coins where denomination = denomin
            String hql = "from Coin where denomination = " + Integer.toString(denomin);
            Query query = session.createQuery(hql);
            listCoins = query.list();

        } catch(Exception e ){
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return listCoins;
    }

    // get coin by metal
    public List<Coin> getCoinsByMetal(String metal) {
        List<Coin> result = new ArrayList<Coin>();
        List<Coin> listCoins = getAllCoins();

        for (Coin coin : listCoins) {
            // look if description contains metal
            if (coin.getDescription().contains(metal)) {
                //System.out.println(coin.getDescription());
                result.add(coin);
            }
        }
        return result;
    }

    // get coin by year & denomination
    public List<Coin> getCoinsByYearDenomination(String date, int denomin) {
        List<Coin> listCoins = null;
        // if
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // select from Coins where denomination = denomin
            String hql = "from Coin where YEAR(coinDate) = " + date +
                                        " and denomination = " + Integer.toString(denomin);
            Query query = session.createQuery(hql);
            listCoins = query.list();

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return listCoins;
    }

    // get coin by metal and year
    public List<Coin> getCoinsByMetalYear(String metal, String date) {

        List<Coin> result = new ArrayList<Coin>();
        List<Coin> listCoins = getCoinsByYear(date);

        for (Coin coin : listCoins) {
            //System.out.println(coin);
            // look if description contains metal if copper-nickel
            if (coin.getDescription().contains(metal)) {
                //System.out.println(coin.getDescription());
                result.add(coin);
            }
        }
        return result;
    }

    // get coin by metal & denomination
    public List<Coin> getCoinsByMetalDenomination(String metal, int denomin) {

        List<Coin> result = new ArrayList<Coin>();
        List<Coin> listCoins = getCoinsByDenomination(denomin);

        for (Coin coin : listCoins) {
            //System.out.println(coin);
            // look if description contains metal if copper-nickel
            if (coin.getDescription().contains(metal)) {
                //System.out.println(coin.getDescription());
                result.add(coin);
            }
        }
        return result;
    }

    // get coin by year & denomination and metal
    public List<Coin> getCoinsByMetalYearDenomination(String metal, String date, int denomin) {

        List<Coin> result = new ArrayList<Coin>();
        List<Coin> listCoins = getCoinsByYearDenomination(date, denomin);

        for (Coin coin : listCoins) {
            //System.out.println(coin);
            // look if description contains metal if copper-nickel
            if (coin.getDescription().contains(metal)) {
                //System.out.println(coin.getDescription());
                result.add(coin);
            }
        }
        return result;
    }

}

