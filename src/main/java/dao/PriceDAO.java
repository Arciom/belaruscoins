package dao;

import coins.Price;
import java.util.List;
import hibernate.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class PriceDAO {

    private Session session;

    public PriceDAO() {

    }
    // adding price
    public void insertPrice(Price price) {

        try {
           session = HibernateUtil.getSessionFactory().openSession();
           session.beginTransaction();
           session.save(price);
           session.getTransaction().commit();

        } catch(Exception e) {
             e.printStackTrace();
        } finally {
           if ((session != null) && (session.isOpen())) {
               session.close();
           }
        }
    }
    // deleting the price by id
    public void deletePrice(int id) {

        try {
           session = HibernateUtil.getSessionFactory().openSession();
           session.beginTransaction();
           Price result = (Price) session.createCriteria(Price.class)
                .add(Restrictions.idEq(id))
                .uniqueResult();

           if (result != null){
              session.delete(result);
              session.getTransaction().commit();
           }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    // updating the price
    public void updatePrice(Price price) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(price);
            session.getTransaction().commit();

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen())) {
                session.close();
            }
        }
    }

    // get all
    public List <Price> getAllPrices() {
        List <Price> listPrices = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            listPrices = session.createCriteria(Price.class).list();

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return listPrices;
    }

    // get prices by coin id
    public List<Price> getPricesByCoinId(int id) {
        List<Price> listPrices = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // select from Prices where id_coin = id
            String hql = "from Price where idCoin = " + Integer.toString(id);
            Query query = session.createQuery(hql);
            listPrices = query.list();

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if ((session != null) && (session.isOpen()))
                session.close();
        }
        return listPrices;
    }

}

