package dao;

public class CoinsDAOFactory {

  public CoinDAO getCoinDAO() {
    //System.out.println("************************getCoin");
    return new CoinDAO();
  }

  public PriceDAO getPriceDAO() {
    //System.out.println("***********************getPrice");
    return new PriceDAO();
  }

}
