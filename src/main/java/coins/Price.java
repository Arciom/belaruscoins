package coins;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "prices" )
public class Price {

    @Id
    @Column(name = "id_price")
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    private Integer idPrice;
    @Column(name = "price")
    private String price;
    @Column(name = "grade")
    private String grade;
    @Column(name = "sold_date")
    private String soldDate;
    @Column(name = "bids")
    private String bids;
    @Column(name = "au_link")
    private String auLink;
    @Column(name = "image")
    private String image;
    @Column(name = "id_coin")
    private int idCoin;

    public Price(int idPrice, String price, String grade, String soldDate, String bids, String auLink, String image, int idCoin) {
        this.idPrice = idPrice;
        this.price = price;
        this.grade = grade;
        this.soldDate = soldDate;
        this.bids = bids;
        this.auLink = auLink;
        this.image = image;
        this.idCoin = idCoin;
    }

    public Price(){
        
    }

    public Integer getIdPrice() {
        return idPrice;
    }

    public void setIdPrice(Integer idPrice) {
        this.idPrice = idPrice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(String soldDate) {
        this.soldDate = soldDate;
    }

    public String getBids() {
        return bids;
    }

    public void setBids(String bids) {
        this.bids = bids;
    }

    public String getAuLink() {
        return auLink;
    }

    public void setAuLink(String auLink) {
        this.auLink = auLink;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getIdCoin() {
        return idCoin;
    }

    public void setIdCoin(int idCoin) {
        this.idCoin = idCoin;
    }

    @Override
    public int hashCode() {
        return idPrice != null ? idPrice.hashCode() : 0;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if (!( o instanceof Price ) ) {
            return false;
        }

        if ( idPrice == null ) {
            return false;
        }

        final Price other = ( Price ) o;
        return idPrice.equals( other.idPrice );
    }

    @Override
    public String toString() {
        return "Price[" +
                "idPrice=" + idPrice +
                ", price='" + price + '\'' +
                ", grade='" + grade + '\'' +
                ", soldDate='" + soldDate + '\'' +
                ", bids=" + bids +
                ", auLink='" + auLink + '\'' +
                ", image='" + image + '\'' +
                ", idCoin=" + idCoin +
                "']\n"
                ;
    }
}
