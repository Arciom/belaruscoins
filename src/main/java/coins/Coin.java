package coins;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "coins" )
public class Coin {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    private Integer id;
    @Column(name = "coin_name")
    private String coinName;
    @Column(name = "denomination")
    private int denomination;
    @Column(name = "description")
    private String description;
    @Column(name = "coin_date")
    private String coinDate;
    @Column(name = "image_obverse")
    private String imageObverse;
    @Column(name = "image_reverse")
    private String imageReverse;

    public Coin(int id, String currency,  int denomination, String description, String coinDate, String imageObverse, String imageReverse) {
        this.id = id;
        this.coinName = currency;
        this.denomination = denomination;
        this.description = description;
        this.coinDate = coinDate;
        this.imageObverse = imageObverse;
        this.imageReverse = imageReverse;
    }

    public Coin(){
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if (!( o instanceof Coin ) ) {
            return false;
        }

        if ( id == null ) {
            return false;
        }

        final Coin other = ( Coin ) o;
        return id.equals( other.id );
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }
    
    public int getDenomination() {
        return denomination;
    }

    public void setDenomination(int denomination) {
        this.denomination = denomination;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoinDate() {
        return coinDate;
    }

    public void setCoinDate(String coinDate) {
        this.coinDate = coinDate;
    }

    public String getImageObverse() {
        return imageObverse;
    }

    public void setImageObverse(String imageObverse) {
        this.imageObverse = imageObverse;
    }

    public String getImageReverse() {
        return imageReverse;
    }

    public void setImageReverse(String imageReverse) {
        this.imageReverse = imageReverse;
    }
    
    @Override
    public String toString() {
	return "[ID=" + this.id + ", CoinName=" + this.coinName + ", Denomination=" + this.denomination +
                ", Description=" + this.description + ", CoinDate=" + this.coinDate +
               ", ImageObverse=" + this.imageObverse  + ", ImageReverse=" + this.imageReverse + "]\n";
    }
}
