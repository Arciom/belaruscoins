<!DOCTYPE html>
<html lang="en">
<head>
    <title>Belarus coins</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
    <script src="scripts/price.js"></script>
    <script src="scripts/coin.js"></script>

    <style>

        body {
            position: relative;
        }
        table {
            cursor: pointer;
        }
        .navbar {
            margin-bottom: 0px;
            opacity: 0.9;
        }
        .navbar-toggle:before {
            content:"Menu";
            left:-50px;
            top:4px;
            position:absolute;
            width:50px;
        }
        .row {
            margin: 5px;
        }
        .panel {
            background-color: #efebe4;
        }
        button {
            margin: 5px;
        }
        #section1 {padding-top:60px;min-height:300px; background-color: #efeee0;}
        #section2 {padding-top:20px; background-color: #d1d0c4;}
        #section3 {padding-top:20px;min-height:400px; background-color: #d5d4c8;}
        #section4 {padding-top:20px; background-color: #efebe4;}
        footer {
            background-color: #fdf8ef;
            padding: 15px;
        }
        /* On small screens, set height to 'auto' for the grid */
        @media screen and (max-width: 767px) {
            .row.content {height: auto;}
        }
    </style>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand">Belarus coins</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#section1">Home</a></li>
                <li><a href="#section2">Coins catalog</a></li>
                <li><a href="#section3">Coins prices</a></li>
                <li><a href="#section4">About</a></li>
            </ul>
        </div>
    </div>
</nav>

<div id="section1" class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <img class="img-responsive" src="images/blr.gif" alt="Coins logo" width="150" >
      </div>
    <div class="col">
      <h3>   Hello, this is web app about Belarus coins.</h3>
      <h4>You can look at belarusian commemorative coins, just click the button "Show all coins",</h4>
      <h4>or filter coins by choosing the metal, denomination or date of the coins.</h4>
      <h4>Then you can find out their prices - clicking on the coins table row.</h4>
      <h4>The prices are based on the results of popular internet-auctions.</h4>
    </div>
  </div>


        <div class="row row-offcanvas row-offcanvas-right">
            <button id="coin2" type="button" class="btn btn-default">Show all coins</button>
        </div>
        <div class="row row-offcanvas row-offcanvas-right">
            <button type="button" class="btn btn-default" data-toggle="collapse" data-target="#filter-panel">
                <span class="glyphicon glyphicon-cog"></span> Coins filter
            </button>
            <!-- panel to be shown -->
            <div id="filter-panel" class="collapse filter-panel">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="form" class="form-inline" role="form">
                            <label class="filter-col" style="margin-right:0;" for="formMetal">Metal:</label>
                            <select id="formMetal" class="form-control">
                                <option selected="selected" value="Any">Any</option>
                                <option value="Gold">Gold</option>
                                <option value="Silver">Silver</option>
                                <option value="Copper">Copper</option>
                            </select>

                            <label class="filter-col" for="formDenom">Denomination:</label>
                            <select id="formDenom" class="form-control">
                                <option selected="selected" value="Any">Any</option>
                                <option value="1">1</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="200">200</option>
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                            </select>

                            <label class="filter-col" for="formYear">Year:</label>
                            <input id="formYear" type="number" name="year" class="form-control">

                            <script>
                                $("form").validate({
                                    rules: {
                                        formYear: {
                                            //required: true,
                                            digits: true
                                        }
                                    },
                                    highlight: function(element) {
                                              $(element).css('background', '#ffdddd');
                                    },
                                    // Called when the element is valid:
                                    unhighlight: function(element) {
                                        $(element).css('background', '#ffffff');
                                    }
                                });
                            </script>

                            <button id="selectedCoins" type="button" class="btn btn-default">Show selected coins</button>
                        </form>
                    </div> <!-- panel body -->
                </div>
            </div> <!-- panel -->
        </div>
    </div>
</div> <!-- section1 -->

<div id="section2" class="container-fluid">
  <div class="container">
    <div id="coinsIndexPage" class="col-xs-12">

    </div>
  </div>
</div>

<div id="section3" class="container-fluid">

  <div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

      <h2 id="s3_name">50th Anniversary WW2 Victory</h2>
      <br>

      <div class="col-xs-6 col-md-4">
        <img id="s3_obverse" class="img-responsive" src="images/coins/1blr1995.png" alt="Coins logo">
      </div>

      <div class="col-xs-6 col-md-4">
         <img id="s3_reverse" class="img-responsive" src="images/coins/2blr1995.png" alt="Coins logo">
      </div>
      <br>
      <div class="col-xs-12 col-md-4">
          <div class="panel panel-default">
            <div id="s3_coindata" class="panel-body">
              <p style="padding-top: 6px; padding-bottom: 6px;vertical-align:top">
              <b> Gold</b>, Alloy standard of gold: 999.9
              <br> Denomination: 1000 (500, 250, 125, 50) rubles
              <br> Weight of coin, g: 31.10 (24.88, 18.66, 12.44, 6.22)
              <br> Quality: "proof"
              <br> Mintage, pcs.: 1,000
              </p>
            </div>
          </div>
      </div><!--/.col-xs-6.col-md-4-->

      <div id="s3_price_table" class="col-xs-12">
        <h4 id="goldCoin"><p>The first Belarus commemorative coins - RARE now.</p>
            <p>They have fabric defects. So the most part of the mintage was withdrawn.</p>
            <p>It's known about 100 coins in numismatic collections.</p></h4>
      </div>

    </div><!--/row-->

    <a href="#section1">Go back to coin choosing</a>

  </div>

</div>

<div id="section4" class="container-fluid">
 <div class="container">
  <div class="panel panel-default">
    <div class="panel-body">

     <p> All coins images and descriptions are from nbrb.by.</p>
     <p> All prices are based on ebay.com auctions.</p>

    </div>
  </div>

  <div class="col-sm-12" id="coinsResults">
  </div>
 </div>
</div>

<footer class="container-fluid text-center">
    <p>&copy; 2016 Alfenid</p>
</footer>

</body>
</html>