$(document).ready(function() {
    $("button").click(function(event) {

        var buttonID = event.target.id;
        // delete old table
        $("#coinsTable").remove();
        // delete old table dataTable() data from "coinsTable_wrapper"
        $("#coinsTable_wrapper").remove();

        var tableCoins = document.createElement("table");
        tableCoins.setAttribute("id", "coinsTable");
        tableCoins.setAttribute("class", "table table-striped table-hover");
        var tableHeadCoins = document.createElement("thead");
        tableCoins.append(tableHeadCoins);
        var tableBodyCoins = document.createElement("tbody");
        tableCoins.append(tableBodyCoins);
        $("<tr>").appendTo(tableHeadCoins)
            .append($("<th data-field='id'>").text("Coin ID"))
            .append($("<th data-field='coinName'>").text("Name"))
            .append($("<th data-field='description'>").text("Description"))
            .append($("<th data-field='coinDate'>").text("Coin Date"))
            .append($("<th data-field='imageObverse'>").text("Coin Obverse"))
            .append($("<th data-field='imageReverse'>").text("Coin Reverse"));

        $("#coinsIndexPage").append(tableCoins);

        var metal = $("#formMetal option:selected").text();
        // to avoid "Gold-platted" coins
        if (metal === "Gold") metal = "Gold</b>";
        var denom = $("#formDenom option:selected").text();
        var year = $("#formYear").val();

        $.getJSON('CoinsServlet', {"buttonId": buttonID, "metalSc": metal, "yearSc": year, "denomSc": denom},
            function(resp) {
                $.each(resp, function (i, coinData) {

                    var imgObv = document.createElement("IMG");
                    imgObv.setAttribute("class", "img-responsive");
                    imgObv.setAttribute("src", "images/coins/" + coinData.imageObverse);
                    imgObv.setAttribute("width", "120");
                    imgObv.setAttribute("height", "120");
                    imgObv.setAttribute("alt", "coin");
                    // to open image in new tab
                    var obvAhref = document.createElement("a");
                    obvAhref.setAttribute("href", "images/coins/" + coinData.imageObverse);
                    obvAhref.setAttribute("target", "_blank");
                    obvAhref.append(imgObv);

                    var imgRev = document.createElement("IMG");
                    imgRev.setAttribute("class", "img-responsive");
                    imgRev.setAttribute("src", "images/coins/" + coinData.imageReverse);
                    imgRev.setAttribute("width", "120");
                    imgRev.setAttribute("height", "120");
                    imgRev.setAttribute("alt", "coinReverse");
                    // to open image in new tab
                    var revAhref = document.createElement("a");
                    revAhref.setAttribute("href", "images/coins/" + coinData.imageReverse);
                    revAhref.setAttribute("target", "_blank");
                    revAhref.append(imgRev);

                    // inserting data to the coinsTable  onclick='onCoinRowClick()'
                    $("<tr id='coinsTableRow' onclick='onCoinRowClick(event)'>").appendTo(tableCoins)
                        .append($("<td id='coinId' width='70'>").text(coinData.id))
                        .append($("<td>").text(coinData.coinName))
                        .append(coinData.description)
                        .append($("<td>").text(coinData.coinDate))
                        .append($("<td id='coinObv'>").append(obvAhref))
                        .append($("<td id='coinRev'>").append(revAhref));
                });
                // turn on table.js power
                $('#coinsTable').dataTable();
            }
        );
    });
// end
});