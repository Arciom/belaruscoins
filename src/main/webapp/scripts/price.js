function onCoinRowClick(event) {
        // as it's clicking on <td> element, look for its parent <tr> for getting 'coinsTableRow'
        // when clicked to
        var tableID = event.target.parentElement.id;
        var cID = event.target.parentElement.children[0].innerText;
        // delete old table
        $("#pricesTable").remove();
        $("#goldCoin").remove();
        // delete old table dataTable() data from "pricesTable_wrapper"
        $("#pricesTable_wrapper").remove();

        // get clicked row's index=1 element's text (it's coinName)
        document.getElementById("s3_name").innerText = event.target.parentElement.children[1].innerText;
        // (children[3] - date of coin) + children[2] - description
        document.getElementById("s3_coindata").innerHTML = "Date of coin: " +
                                                            event.target.parentElement.children[3].innerText +
                                                            "<br>" + event.target.parentElement.children[2].innerHTML;
        document.getElementById("s3_obverse").setAttribute("src", event.target.parentElement.children[4].firstChild.href);
        document.getElementById("s3_reverse").setAttribute("src", event.target.parentElement.children[5].firstChild.href);

        // create table
        var tablePrice = document.createElement("table");
        tablePrice.setAttribute("id", "pricesTable");
        tablePrice.setAttribute("class", "table table-striped table-hover");
        var tableHead = document.createElement("thead");
        tablePrice.append(tableHead);
        var tableBody = document.createElement("tbody");
        tablePrice.append(tableBody);
        $("<tr>").appendTo(tableHead)
            .append($("<th data-field='N'>").text("N"))
            .append($("<th data-field='price'>").text("Price"))
            .append($("<th data-field='grade'>").text("Grade"))
            .append($("<th data-field='bids'>").text("Bids"))
            .append($("<th data-field='soldDate'>").text("Sold date"))
            .append($("<th data-field='auLink'>").text("Auction"))
            .append($("<th data-field='image'>").text("Image"));

        $("#s3_price_table").append(tablePrice);

        // send to servlet two parameters tableID to know that it's on coinsTable click &cID
        $.getJSON('CoinsServlet', {"tableId": tableID, "cId": cID},
            function(resp2) {
                $.each(resp2, function(i, priceResp) {

                    var priceImg = document.createElement("img");
                    priceImg.setAttribute("class", "img-responsive");
                    priceImg.setAttribute("src", "images/prices/" + priceResp.image);
                    priceImg.setAttribute("width", "40");
                    priceImg.setAttribute("height", "40");
                    priceImg.setAttribute("alt", "image");
                    // to open image in new tab
                    var imgAhref = document.createElement("a");
                    imgAhref.setAttribute("href", "images/prices/" + priceResp.image);
                    imgAhref.setAttribute("target", "_blank");
                    imgAhref.append(priceImg);

                    // to do auction link
                    var auhref = document.createElement("a");
                    auhref.setAttribute("href", priceResp.auLink);
                    auhref.setAttribute("target", "_blank");
                    auhref.innerText = "Auction link";

                    // inserting data to the table
                    $("<tr>").appendTo(tablePrice)
                        .append($("<td id='Id' width='70'>").text(++i))
                        .append($("<td>").text(priceResp.price))
                        .append($("<td>").text(priceResp.grade))
                        .append($("<td>").text(priceResp.bids))
                        .append($("<td>").text(priceResp.soldDate))
                        .append($("<td>").append(auhref))
                        .append($("<td id='priceImage'>").append(imgAhref))
                    ;
                });
                // turn on table.js power
                $('#pricesTable').dataTable();
                window.location = "#section3";
            });
};