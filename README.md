Belarus coins web application.

http://belaruscoins.eu-west-1.elasticbeanstalk.com

Catalog of belarusian commemorative coins and their prices.
The prices are based on the results of popular internet-auctions.

Tools and environment: Java, MySQL, Hibernate, Spring, Tomcat, Maven, Git.